from setuptools import setup

SETUP_INFO = dict()
SETUP_INFO.update(url='https://gitlab.com/lino-framework/cg')
if __name__ == '__main__':
    setup(**SETUP_INFO)
