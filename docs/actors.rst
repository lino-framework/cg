================
Actors
================

The members of the :term:`Lino community` can be categorized according to their
motivations and responsibilities.

In a simple partnership you can see the :term:`site operator` as the customer of
a :term:`solution provider`.


.. toctree::
   :maxdepth: 1

   so
   sp
   sm
   hp
   ac
   dp
