.. _community:
.. _cg:

Welcome to the **Lino Community Guide**, the website for everybody who wants
to interact with the :term:`Lino community`.

Lino Community Guide
====================


.. toctree::
  :maxdepth: 2

  actors
  topics/index
  copyright
