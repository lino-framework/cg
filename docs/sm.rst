=====================
Server administrators
=====================

The :term:`server administrator` communicates with the :term:`site operator(s)
<site operator>`, the :term:`server provider` and the :term:`application
developer`.

The :term:`server administrator` organizes access to locally customized document
templates to the :term:`site manager`.

A :term:`server administrator` knows how to install Python packages (using `pip
<https://pip.pypa.io/en/stable/>`__ into virtual environments (using `virtualenv
<https://virtualenv.pypa.io/en/stable/index.html>`__).  Previous experience with
hosting `Django <https://www.djangoproject.com/>`_ applications is useful.

A :term:`server administrator` is not a :term:`software developer` and does not
need profound knowledge of Lino or the Python language.
