====================
Service sales models
====================

There are two types of sales models for services : :term:`flat-rate support` and
:term:`per-hour support`.

.. glossary::

  flat-rate support

    A support sales mode where the customer pays a given sum for a given period
    and gets unlimited support.  The only limit are human resources. The
    provider promises that they give their best to help the customer with any
    problem. Upon agreement the provider can write additional invoices for extra
    work that needs more effort than usual.

    This model works well when provider and customer trust each other and want a
    long-term relationship. The project is seen as a collaboration where both
    partners contribute their work. Advantage is reduced administrative cost and
    increased communication.

  per-hour support

    A support sales mode where the customer pays for every hour of work required
    by the provider.  The provider is responsible for writing service reports.
    Advantage is transparency.
