=========================
Components of a Lino site
=========================

About sites and applications
============================

.. glossary::

  Lino site

    An instance of a given :term:`Lino application` running on a given
    :term:`server <Lino server>`.

  Lino application

    A :term:`database application` that was developed using the Lino framework.

    See also :ref:`getlino.apps`

Every :term:`Lino site` is owned by its :term:`site operator` who manages
:term:`user accounts <user account>` of their site and assigns permissions. The
:term:`site operator` owns the data on their site. The :term:`site operator`
designates a :term:`server administrator` who is responsible for installing and
updating the software on the site.

A :term:`Lino site` can be used either for internal use by your employees in
your local network, or as a public website. All Lino sites have a similar,
typical, look and feel, which is optimized for efficient daily usage.

.. With Lino
  you don't want to care (and to pay) for a customized look and feel of your site.
  To be exact, there are currently two choices regarding the look and feel of your
  site. You can choose (and easily switch) between two web front ends: the classic
  one based on **ExtJS** and the younger on based on **ReactJS**.

Every :term:`Lino site` has a web interface under a domain or subdomain name, a
set of local settings and configuration files, and usually its own database.
Multiple sites can share a same database in order to provide different
:term:`front ends <front end>`.

Every :term:`Lino site` runs a given :term:`Lino application`.

Every :term:`Lino application` is a :term:`software product` on its own. It has
a given set of functionalities and can be recognized by its :term:`end users
<end user>`.

A same Lino application can potentially run on many sites. In that case the
operators of these sites automatically have a common  :term:`product carrier`.

The :term:`product carrier` of an application decides which features and
functionalities are available in their application: the **database structure**
(how data is stored), the **view layouts** (how end users see the data) and the
**actions** (what users can do with the data).

A Lino consultant or distributor may decide to run one or several :term:`online
demo sites <online demo site>`.

.. glossary::

  online demo site

    A fully functional :term:`Lino site` with fictive demo data used for showing
    Lino to other people. All online demo sites  get updated and reinitialized
    every morning. You may edit according to the user permissions, but your
    changes will have vanished the next day. The sites are visible and writeable
    for everybody, so don't enter any sensitive private information there.


.. _cg.repositories:

Code repositories
=================

Because Lino is :term:`Free Software`, its source code is publicly available for
everybody.

The Lino framework is stored on `GitLab <https://gitlab.com/lino-framework>`__
as a set of about 30 public :term:`code repositories <code repository>`.

We differentiate the following types of repositories:
the :term:`Lino core`,
the :term:`Lino Extensions Library`,
a series of :ref:`Lino applications <getlino.apps>`,
a series of :term:`front ends <front end>`
and a series of :term:`documentation trees <documentation tree>`.

See :ref:`dev.overview` for a more technical overview.


.. glossary::

  source repository

      A set of :term:`source files <source file>` that is grouped together as
      a whole and published together with a version control system.

  source code

    Content to be edited by a :term:`software developer` and to be built
    (compiled) into an executable program file or consumable content (e.g. text,
    image, sound or video).

  source file

    A file that contains :term:`source code`.

    Examples of source file types commonly used for building executable
    program code are :file:`.py`, :file:`.js` and :file:`.rst`.

    Some consumable content file formats commonly used in a Lino project
    are :file:`.html` and :file:`.pdf`.

  configuration file

    A file that contains configuration settings to be read by a program.


  plugin

      A module or logical part of an application that potentially can be shared
      among several applications.

  front end

    A specialized :term:`plugin` responsible to render the web interface of a
    :term:`Lino site`.

    A :term:`Lino site` can be exposed with different front ends, using the same
    application code and database but different front ends.

Lino contains roughly 4500 source files with Python code (:file:`.py`) and 6500
source files with Sphinx code (:file:`.rst`).

Some more jargon you might encounter when talking to a :term:`software
developer`:

.. glossary::

    test suite

      A set or :term:`source code` files that don't add any functionality and is
      used only for :term:`automated testing`.
