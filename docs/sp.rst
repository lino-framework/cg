==================
Solution providers
==================

.. glossary::

  solution provider

    A service provider who is able to provide all the services needed by a
    :term:`site operator`.

A solution provider usually connects with other service providers, but for
the :term:`site operator` he is the sole service vendor and legally
responsible person.

.. glossary::

  support

    The service of helping somebody with using a given system.

    Support --unlike research-- is always a response to a concrete
    :term:`support request`.

    Can be done professionally by a :term:`support provider`.

  support provider

    A :term:`service provider` who provides :term:`support` to their customer.

    A support provider, a side effect, usually also publishes and maintains
    documentation.

  support request

    A request for help submitted by a customer to a :term:`support provider`.

  end-user support

    Basic first-level support to :term:`end users <end user>`.

    The support provider is always available when end users need them.
    Forwards the request other actors if they cannot help directly.

  end-user training

    A service that comprises publishing of training material and/or organizing
    training sessions for :term:`end users <end user>`.
