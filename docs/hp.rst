=================
Hosting providers
=================

We maintain a list of recommended :ref:`Lino hosting providers
<hosting_provider>`.


.. glossary::

  hosting provider

    A legal person who provides :term:`hosting service`.

  hosting service

    The work of installing and maintaining a :term:`Lino site` for a :term:`site
    operator`.

    The combined services of a :term:`server provider` and a :term:`server
    administrator`.

  server provider

    Installs and maintains a virgin Linux server to be used for running
    :term:`Lino sites <Lino site>`.

    Is responsible for root actions, backups, reliable functioning and
    protection against certain types of attacks unauthorized access.

    Creates one or several SSH user accounts to be used by the :term:`server
    administrator`.  Grants access to the server via SSH.

    Makes the web sites on the server available via Internet or a local
    network.

  server administrator

    Installs and maintains the software on a :term:`Lino server`.

    Takes care of the maintenance and security of the server.
    Plans and executes software updates and data migrations.

    See :doc:`/sm`.


Responsibilities of a hosting provider
======================================

As a :term:`Lino hosting provider <hosting provider>` you assume the following
responsibilities:

- You set up a virtual machine running with a virgin Debian operating system and
  grant SSH access to a :term:`server administrator`.

- You care about registering a domain name and SSH certificates if the
  customer needs it.

- You care about security and protect the server against hackers

- You make backups of the server to make sure it doesn't get lost in
  case of a serious accident.

- You care about scaling. When your customer's site grows, they might want to
  move to a bigger machine.

- You care about reliability and make sure that the Lino site is
  always available to respond when your customer needs it.

- You respond to :term:`support requests <support request>` of the :term:`site
  operator`.





..
    The :term:`hosting provider` is also the :term:`application developer`.
    i.e. they answer end-user questions about how to use or configure the
    software, and they are able upgrade the site when new versions of the
    software are available. They forward any reported
    problems to the responsible application or core developer.

.. development hosting

..      The :term:`hosting provider` additionally provides :term:`expert support` and
        :term:`server administration <server administrator>`

..
  In case of **server hosting** the server operator has two contracts: one with a
  developer and one with a hosting provider.

  Your job is to provide and manage the server where the developer will
  install and maintain Lino. You make sure that the server is available
  and secure. You collaborate with the developer for certain tasks like
  mail server setup.

  You are *not* reponsible for maintaining the system software on that
  server, nor answering end-user questions about how to use or configure
  the software. That's the job of the developer.

  You are able to act as :term:`server administrator`.

  It is also your job to decide whether and when you are able to offer **stable
  hosting** for one or several Lino applications.

  The difference between development and application hosting is that your
  emergency maintainer has grown into an independent maintainer who can
  maintain the system software, give limited end-user support and
  install new versions of the application when the customer asks you to
  do so.  In stable mode, the customer pays more money to you because
  you provide additional services and because they don't need support by
  a developer.  With stable hosting, no external developer has access to
  your customer's server.

  In case of **development hosting** you offer both the hosting and the
  development.


..
    A **master machine** is a virtual machine which hosts one or several
    demo sites on different Lino versions.

    customized for you by a
    developer

    You can set up and maintain a docker server and serve one of the
    dockerfiles maintained by the Lino team.  See e.g.
    https://docs.docker.com/engine/installation/linux/ubuntulinux/

    With Docker hosting the customer is always in stable mode and cannot
    switch to development mode.

    The Lino team plans to start this type of hosting as soon as there is
    a first pilot user.
