====================
Product carriers
====================

This document describes the responsibilities of a :term:`product carrier`.

.. glossary::

  product carrier

    The legal or natural person who *carries* or *governs* the usage of a
    :term:`software product`.


A :term:`product carrier` invests into development, maintenance and marketing of
a given :term:`software product`.

- Decides about the product's lifetime, usage conditions,
  functionalities and priorities of development.

- Represents the community of its users.

- Plans the :term:`software product` and decides about its destiny.

- Decides which features to add or to remove.

- Does strategic decisions.

- Provides a :term:`product manager` and a :term:`development provider`.

- May provide :term:`expert support` service to :term:`site operators <site operator>`.

When a given :term:`Lino application` is being used on only one site, the
:term:`operator <site operator>` of that site is de facto also the
:term:`application developer`.  Otherwise the application developer is a
separate legal person by which several site operators collaborate and maintain
the application as their shared project.


.. glossary::

  product manager

    Finds new business partners.

    Explores the market and formulates strategic choices

  application expert

    Centralizes the requirements of all users and negotiates priority conflicts.

    :term:`site experts <site expert>`
