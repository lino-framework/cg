# -*- coding: utf-8 -*-
from atelier.sphinxconf import configure

configure(globals())
from lino.sphinxcontrib import configure

configure(globals())

# General substitutions.
project = "Lino Community Guide"
import datetime

copyright = '2019-{} Rumma & Ko Ltd'.format(datetime.date.today().year)
html_title = "Lino Community Guide"
htmlhelp_basename = 'cg'
extensions += ['lino.sphinxcontrib.logo']

html_context.update({
    'display_gitlab': True,
    'gitlab_user': 'lino-framework',
    'gitlab_repo': 'cg',
})

# html_theme_options.update(globaltoc_includehidden=False)

inheritance_graph_attrs = dict(rankdir="TB")
# inheritance_graph_attrs.update(size='"12.0, 16.0"')
inheritance_graph_attrs.update(size='"48.0, 64.0"')
inheritance_graph_attrs.update(fontsize=14, ratio='compress')

# intersphinx_mapping['lf'] = ('https://www.lino-framework.org/', None)
# intersphinx_mapping['ug'] = ('https://using.lino-framework.org/', None)
# intersphinx_mapping['hg'] = ('https://hosting.lino-framework.org/', None)
# intersphinx_mapping['book'] = ('https://dev.lino-framework.org/', None)

if False:
    # extensions += ['yasfb']
    # extensions += ['sphinxcontrib.feed']
    extensions += ['sphinxfeed']
    # NB : not the public sphinxfeed but my extended version
    feed_base_url = 'https://community.lino-framework.org'
    feed_author = 'Luc Saffre'
    feed_title = "Lino Community Guide"
    feed_field_name = 'date'
    feed_description = "News about the Lino Community"

# extensions += ['hieroglyph']  # Generate HTML presentations
# autoslides = False
# slide_numbers = True

import os

os.environ['LC_TIME'] = 'de_BE.UTF-8'

# from pprint import pprint
# pprint(intersphinx_mapping)
#
# raise foo

# language = 'en'
# locale_dirs = ['locales/']
# gettext_compact = False
# translated_languages = ['de', 'fr']  # used by atelier

html_use_index = True

rst_prolog = """

:doc:`/actors` | :doc:`/topics/index` |

"""
