.. _developers:

==========================
Development providers
==========================

.. contents::
   :depth: 1
   :local:

Developers
==========

.. glossary::

  software developer

    Somebody who develops a :term:`software product`.
    Depending on the context, this can mean either a natural or a legal person.

    Software development includes one or more of the following tasks:
    :term:`analysis`,
    :term:`programming`,
    :term:`testing`,
    :term:`deployment`,
    :term:`maintenance` and/or
    :term:`support`.

We differentiate the following types of Lino developers:

.. glossary::

  application developer

    A :term:`software developer` who writes and maintains the :term:`source
    code` of a given :term:`Lino application`.

  core developer

    A :term:`software developer` who helps maintaining the :ref:`Lino framework
    <lf>` as a whole, which includes maintaining the :term:`privileged
    applications`.

  contributing developer

    An :term:`application developer` who contributes to the :ref:`Lino framework
    <lf>`, for example by testing general framework features, discussing changes
    and new features, submitting pull requests, ...


A :term:`development provider` operates a collaboration infrastructure for their
workers. Some of these workers are :term:`developers <software developer>`.

A developer writes and maintains :term:`developer documentation` and a
:term:`test suite` for the application.

A developer writes :term:`release notes` for :term:`site experts <site expert>`.


.. glossary::

  core team

    The people who :term:`develop <software developer>` the :ref:`Lino framework
    <lf>` as a :term:`software product`.


A big business
==============

.. glossary::

  development provider

    An organisation that provides professional services in order to develop and
    maintain a :term:`software product`.

  software engineer

    A natural person who is not a :term:`programmer <programming>` but an active
    member of a developer team.



What a software developer does
==============================

A :term:`development provider` can provides the following services.

.. glossary::

  analysis

    The art of formulating the needs of a customer in a language that
    can be understood by a :term:`programmer <programming>`.

  programming

    Write and maintain the :term:`source files <source file>` and publish them.

  testing

    Make sure that the software does what it is meant to do, that a new version
    does not introduce regressions or other side effects. This is also called
    *quality control*.

  deployment

    Installing the software on a remote site, either public or for a
    :term:`site operator`.

  developer support

    Support given to an :term:`application developer`.

  expert support

    Support given by a :term:`development provider` to a :term:`site
    expert`. This is more technical and specialized than :term:`end-user
    support`.

  manual testing

    A method of software testing where the testers manually execute test cases
    without using any automation tools in order to find bugs in the software
    system.  It is imperative for every release because full automated testing
    is not possible. Manual testing is usually done by experienced end users
    because it requires a good knowledge of the functional requirements.
    Optionally they can be executed by the :term:`application developer`
    before a release, or by the :term:`server administrator` after an upgrade.

  automated testing

    A part of the development process which verifies that a change in the
    software doesn't break any existing functionality.

External links
==============

`Programmer Vs Developer Vs Engineer
<https://medium.com/shakuro/programmer-vs-developer-vs-engineer-91ef374e5033>`__

`Programmer vs developer: A recruiter’s guide to telling them apart
<https://devskiller.com/programmer-vs-developer/>`__
