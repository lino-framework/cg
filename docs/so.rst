==============
Site operators
==============

.. glossary::

  site operator

    The legal person that operates a :term:`Lino site`.

The :term:`site operator` owns the data stored on that site and is responsible
for protecting that data against privacy issues.

The :term:`site operator` designates a :term:`hosting provider`, a :term:`server
administrator` and eventually an :term:`application developer` and can reassign
these responsibilities at any time to another :term:`service provider`.

The :term:`site operator` is responsible for granting and revoking of access
rights to :term:`end users <end user>` as well as their :term:`training
<end-user training>` and :term:`support <end-user support>`.

Provides the following human resources either in-house or via a third-party
service provider:

- a :term:`site expert` and a :term:`release manager`.

- :term:`end users <end user>`, :term:`key users <key user>` and
  a :term:`site manager`.



.. glossary::

  site user

      A human who has a :term:`user account` on a given :term:`Lino site`.

  end user

      A :term:`site user` who uses a :term:`Lino site` for actually *working*.
      This is in contrast to a :term:`site manager` who *manages* those who
      work.

  key user

      An experienced :term:`end user` who knows how to use a given part of the
      application, who can explain this to their colleagues and who can give
      basic :term:`end-user support`. If needed, submits support requests to the
      :term:`site expert`.

  site manager

      Manages a :term:`Lino site` via its web interface.

      The :term:`site manager` manages accounts and access permissions to the
      :term:`end users <end user>`, can change site-wide configuration settings
      and has full access to all functions provided via the web interface.

      Don't mix up :term:`site manager` with
      :term:`system administrator`,
      :term:`server administrator` or :term:`hosting provider`.

  system administrator

      Manages the IT system of a :term:`site operator`. Installs,
      configures and maintains :term:`client devices <client device>`
      and software as required.

  release manager

      The contact person between the :term:`site operator` and the :term:`server
      administrator`.

      Coordinates the activities before and after a :term:`site upgrade`.

  site expert

    The contact person between the :term:`site operator` and the service
    providers involved with the :term:`Lino site`.

    Responds to :term:`end-user support` requests reported by :term:`key users
    <key user>`.

    Communicates as needed with the :term:`hosting provider` and the
    :term:`application developer`.

    Formulates and explains the requirements of the :term:`site operator`
    regarding the :term:`Lino application`.

    Introduces :term:`expert support` requests to the :term:`application
    developer` and answers to callback questions.

    Communicates with the :term:`end users <end user>` in order to analyse their
    needs, and then explains to the :term:`application developer` how to make or
    improve the application.

    Organizes training for :term:`key users <key user>`.
